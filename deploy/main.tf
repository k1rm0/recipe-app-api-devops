terraform {
  backend "s3" {
    bucket         = "recipe-app-api-devops-tfstate2209"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = var.aws_region
  version = "~> 2.50.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}

# https://gitlab.com/LondonAppDev/recipe-app-api-devops-course-material/-/snippets/1945376
data "aws_caller_identity" "current" {}
